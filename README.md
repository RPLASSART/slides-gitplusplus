# slides-gitplusplus

![Build Status](https://gitlab.com/le-garff-yoann/slides-gitplusplus/badges/master/build.svg)

From Git to CI/CD with GitLab.

```bash
npm install reveal-md -g

reveal-md index.md
```
